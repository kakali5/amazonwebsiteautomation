*** Settings ***

Library    SeleniumLibrary
Resource    AmazonActions.robot
Resource    PropertiesFile.robot


*** Keywords ***
01_TestCase
    [Arguments]    ${category}    ${subCategory}    ${product}    ${searchName}    ${sortByOptions}
    open browser    ${url}   ${browser}
    maximize browser window
    Sleep    10
    insideamazon    ${category}    ${subCategory}    ${product}    ${searchName}    ${sortByOptions}