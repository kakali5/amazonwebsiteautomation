*** Settings ***
Documentation    Amazon Product Selection Functionality
Library    SeleniumLibrary
Resource    AmazonPageObjects.robot

*** Keywords ***
insideamazon
    [Arguments]    ${category}    ${subCategory}    ${product}    ${searchName}    ${sortByOptions}
    Wait Until Page Contains Element    ${homePage}
    Wait Until Page Contains Element    ${mobileXpath}
    Click Element    ${mobileXpath}
    Wait Until Element Is Visible    ${mobileAndAndAccessories}
    Mouse Over    ${mobileAndAndAccessories}
    Set Selenium Implicit Wait    10


    @{categoryListSelect}=  Get WebElements  ${categoryList}
    FOR     ${category}     IN      @{categoryListSelect}
        ${categoryName}=  Get Text  ${category}
        IF      '${categoryName}' == '${product}'
            Scroll Element Into View  ${category}
            Click Element  ${category}
            Exit For Loop
        END
    END

    Scroll Element Into View    ${primeCheckbox}
    Click Element    ${primeCheckbox}
    Input Text    ${searchBox}    ${searchName}
    Click Button    ${searchEnterButton}
    Set Selenium Implicit Wait    10
    Click Element    ${sortByDropdown}

    @{sortByElements}=    Get Webelements    ${sortByOption}
    FOR     ${sortByElement}     IN      @{sortByElements}
            ${optionsName}=  Get Text  ${sortByElement}
        IF      '${optionsName}' == '${sortByOptions}'
            Scroll Element Into View  ${sortByElement}
            Click Element  ${sortByElement}
            Exit For Loop
        END
    END
    
    @{resultItems}=    Get Webelements    ${resultItems}
    ${productName}=    Get Text   ${resultItems}[0]
    Log    ProductName: ${productName}
    ${price}=    Get Text    ${resultItems}[0]
    Log    ProductPrice: ${price}
    Click Element    ${resultItems}[0]
    Switch Window  NEW
    Wait Until Element Is Visible    ${buyNowButton}    60s
    Scroll Element Into View    ${buyNowButton}      
    Click Button    ${buyNowButton}
    Wait Until Element Is Visible    ${signIn}    50s
    
    













