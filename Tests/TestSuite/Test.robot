*** Settings ***
Library           SeleniumLibrary
Resource        AmazonStepDefinitions.robot
Library           DataDriver    file=${CURDIR}${/}../../TestData/TestDetails.csv    encoding=utf-8    dialect=excel
Test Template    Generic
Default Tags      Test

*** Test Cases ***
Validate ${Test Case Name}    

*** Keywords ***
Generic
    [Arguments]    ${Test Case Name}    ${Execute}    ${category}    ${subCategory}    ${product}    ${searchName}    ${sortByOptions}
    ${type}=    Set Variable    ${Test Case Name}
    Run Keyword if    '${Execute}'=='Y' and '${type}'=='1'    Run Keywords    Log    ${type}
    ...    AND    01_TestCase   ${category}    ${subCategory}    ${product}    ${searchName}    ${sortByOptions}
    ...    ELSE IF    '${Execute}'=='N'    Run Keywords    Skip
