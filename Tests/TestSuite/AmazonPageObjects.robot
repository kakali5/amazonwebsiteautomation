*** Variables ***

${homePage}    //a[@id='nav-logo-sprites']
${mobileXpath}    //*[text()='Mobiles']
${mobileAndAndAccessories}    (//a[@class='nav-a nav-hasArrow'])[1]
${categoryList}    //ul[@class='mm-category-list']/li
${headSetXpath}    //a[text()='Headsets']
${primeCheckbox}    //i[@aria-label='Prime Eligible']
${searchBox}    //input[@id='twotabsearchtextbox']
${searchEnterButton}    //input[@id='nav-search-submit-button']
${sortByDropdown}    //span[@class='a-dropdown-label']
${sortByOption}    //li[@role='option']
${resultItems}    //div[@class='a-section a-spacing-small puis-padding-left-small puis-padding-right-small']
${buyNowButton}    //input[@id='buy-now-button']
${signIn}    //h1[@class='a-spacing-small']